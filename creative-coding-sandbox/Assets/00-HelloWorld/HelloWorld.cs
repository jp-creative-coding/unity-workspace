﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloWorld : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        SayHello();
    }

    private void SayHello()
    {
        Debug.Log("Hello World!");
    }
}
